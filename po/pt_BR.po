# Portuguese translations for gfeeds package.
# Copyright (C) 2019 THE gfeeds'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gfeeds package.
# Gleisson Jesuino Joaquim Cardoso <gleissoncg2@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: gfeeds 0.10\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-20 11:01-0300\n"
"PO-Revision-Date: 2020-11-20 11:53-0300\n"
"Last-Translator: Gleissonjoaquim3 <gleissoncg2@gmail.com>\n"
"Language-Team: Brazilian Portuguese <gleissoncg2@gmail.com>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 2.2.1\n"

#: ../gfeeds/feeds_manager.py:75
#, python-brace-format
msgid "Feed {0} exists already, skipping"
msgstr "A fonte RSS {0} já existe, pulando"

#: ../gfeeds/build_reader_html.py:98
msgid "Thumbnail"
msgstr "Thumbnail"

#: ../gfeeds/headerbar.py:162
msgid "Add new feed"
msgstr "Adiciona nova fonte RSS"

#: ../gfeeds/get_favicon.py:70
#, python-brace-format
msgid "Error downloading favicon for `{0}`"
msgstr "Erro ao baixar o favicon para `{0}`"

#: ../gfeeds/rss_parser.py:61
#, python-brace-format
msgid "Error: unable to parse datetime {0} for feeditem {1}"
msgstr "Erro: Não é possível analisar a data e hora {0} para o item {1}"

#: ../gfeeds/rss_parser.py:169
#, python-brace-format
msgid "Errors while parsing feed `{0}`"
msgstr "Erros ao analisar a fonte RSS `{0}`"

#: ../gfeeds/rss_parser.py:209
#, python-brace-format
msgid "`{0}` may not be an RSS or Atom feed"
msgstr "`{0}` pode não ser uma fonte RSS ou Atom"

#: ../gfeeds/confirm_add_dialog.py:16
msgid "Do you want to import these feeds?"
msgstr "Deseja importar essas fontes RSS ?"

#: ../gfeeds/confirm_add_dialog.py:17
msgid "Do you want to import this feed?"
msgstr "Deseja importar a fonte RSS ?"

#: ../gfeeds/__main__.py:295
msgid "url"
msgstr "url"

#: ../gfeeds/__main__.py:298
msgid "opml file local url or rss remote url to import"
msgstr "url local do arquivo opml ou URL remoto rss para importar"

#: ../gfeeds/download_manager.py:87
#, python-brace-format
msgid "`{0}`: connection timed out"
msgstr "`{0}`: tempo de conexão excedido"

#: ../gfeeds/download_manager.py:89
#, python-brace-format
msgid "`{0}` is not an URL"
msgstr "`{0}` não é um URL"

#: ../gfeeds/download_manager.py:114
#, python-brace-format
msgid "Error downloading `{0}`, code `{1}`"
msgstr "Erro ao baixar `{0}`, código `{1}`"

#: ../gfeeds/settings_window.py:139
msgid "General"
msgstr "Geral"

#: ../gfeeds/settings_window.py:143
msgid "General Settings"
msgstr "Configurações gerais"

#: ../gfeeds/settings_window.py:146
msgid "Refresh articles on startup"
msgstr "Atualiza artigos no início"

#: ../gfeeds/settings_window.py:151
msgid "Show newer articles first"
msgstr "Mostrar novos artigos primeiros"

#: ../gfeeds/settings_window.py:156
msgid "Open links in your browser"
msgstr "Abrir links no  navegador"

#: ../gfeeds/settings_window.py:166
msgid "Maximum article age"
msgstr "Data máxima do artigo"

#: ../gfeeds/settings_window.py:170
msgid "In days"
msgstr "Em dias"

#: ../gfeeds/settings_window.py:176
msgid "Cache"
msgstr "Histórico"

#: ../gfeeds/settings_window.py:179
msgid "Clear all caches"
msgstr "Limpar todo histórico"

#: ../gfeeds/settings_window.py:180
msgid "Clear caches"
msgstr "Limpar histórico"

#: ../gfeeds/settings_window.py:218
msgid "View"
msgstr "Aparência"

#: ../gfeeds/settings_window.py:222
msgid "View Settings"
msgstr "Configurações da aparência"

#: ../gfeeds/settings_window.py:225
msgid "Show full articles titles"
msgstr "Mostrar todo o título dos artigos"

#: ../gfeeds/settings_window.py:230
msgid "Show full feeds names"
msgstr "Mostrar nome completo do feed"

#: ../gfeeds/settings_window.py:235
msgid "Use dark theme for reader mode"
msgstr "Usar tema escuro pra o modo leitor"

#: ../gfeeds/settings_window.py:240
msgid "Enable JavaScript"
msgstr "Ativar JavaScript"

#: ../gfeeds/settings_window.py:256
msgid "Advanced"
msgstr "Avançado"

#: ../gfeeds/settings_window.py:260
msgid "Advanced Settings"
msgstr "Configurações avançadas"

#: ../gfeeds/settings_window.py:263
msgid "Maximum refresh threads"
msgstr "Máximo de tópicos atualizados"

#: ../gfeeds/settings_window.py:268
msgid "How many threads to refresh feeds"
msgstr "Quantidade de fontes RSS a serem atualizadas ao mesmo tempo"

#: ../gfeeds/suggestion_bar.py:39
msgid "There are some errors"
msgstr "Existem alguns erros"

#: ../gfeeds/suggestion_bar.py:48
msgid "Show"
msgstr "Mostrar"

#: ../gfeeds/suggestion_bar.py:49
msgid "Ignore"
msgstr "Ignorar"

#: ../gfeeds/suggestion_bar.py:72
msgid "There were problems with some feeds. Do you want to remove them?"
msgstr "Ocorreram problemas com alguns feeds. Você quer removê-los?"

#: ../gfeeds/suggestion_bar.py:92
msgid "You are offline"
msgstr "Você está sem internet"

#: ../gfeeds/sidebar.py:181
msgid "Feed"
msgstr "Artigos RSS"

#: ../gfeeds/sidebar.py:187
msgid "Saved"
msgstr "Salvos"

#: ../gfeeds/spinner_button.py:16
msgid "Refresh feeds"
msgstr "Atualizar artigos RSS"

#: ../gfeeds/opml_manager.py:42
msgid "Error: OPML path provided does not exist"
msgstr "Erro: O caminho OPML fornecido não existe"

#: ../gfeeds/opml_manager.py:51
#, python-brace-format
msgid "Error parsing OPML file `{0}`"
msgstr "Erro ao analisar o arquivo OPML `{0}`"

#: ../gfeeds/sidebar_row_popover.py:47 ../gfeeds/sidebar_row_popover.py:92
msgid "Mark as unread"
msgstr "Marcar como não lido"

#: ../gfeeds/sidebar_row_popover.py:55 ../gfeeds/sidebar_row_popover.py:82
msgid "Mark as read"
msgstr "Marcar como lido"

#: ../gfeeds/webview.py:74
msgid "Web View"
msgstr "Visualização Web"

#: ../gfeeds/webview.py:75
msgid "Filler View"
msgstr "Visualização de preenchimento"

#: ../gfeeds/webview.py:136
msgid "RSS content or summary not available for this article"
msgstr "Conteúdo ou resumo de RSS não disponível para este artigo"

#: ../gfeeds/opml_file_chooser.py:7
msgid "Choose an OPML file to import"
msgstr "Escolha um arquivo OPML para importar"

#: ../gfeeds/opml_file_chooser.py:14 ../gfeeds/opml_file_chooser.py:31
msgid "XML files"
msgstr "Arquivos XML"

#: ../gfeeds/opml_file_chooser.py:22
msgid "Choose where to save the exported OPML file"
msgstr "Escolha onde salvar o arquivo OPML exportado"

#: ../gfeeds/manage_feeds_window.py:210 ../gfeeds/manage_feeds_window.py:325
msgid "Manage Feeds"
msgstr "Gerenciar fontes RSS"

#: ../gfeeds/manage_feeds_window.py:217
msgid "Select/Unselect all"
msgstr "Marca / Desmarca tudo"

#: ../gfeeds/manage_feeds_window.py:223
msgid "Delete selected feeds"
msgstr "Excluir"

#: ../gfeeds/manage_feeds_window.py:230
msgid "Manage tags for selected feeds"
msgstr "Gerenciar tags para os feeds selecionados"

#: ../gfeeds/manage_feeds_window.py:296
msgid "Do you want to delete these feeds?"
msgstr "Deseja excluir ?"

#: ../gfeeds/feeds_view.py:18
msgid "All feeds"
msgstr "Mostrar todos"

#: ../data/ui/empty_state.glade:42
msgid "Let's get started"
msgstr "Vamos começar"

#: ../data/ui/empty_state.glade:88
msgid "Add new feeds via URL"
msgstr "Adiciona fontes RSS via URL"

#: ../data/ui/empty_state.glade:100
msgid "Import an OPML file"
msgstr "Importa um arquivo OPML"

#: ../data/ui/shortcutsWindow.xml:13
msgid "Open Keyboard Shortcuts"
msgstr "Abrir atalhos do teclado"

#: ../data/ui/shortcutsWindow.xml:19
msgid "Open Menu"
msgstr "Abrir Menu"

#: ../data/ui/shortcutsWindow.xml:25
msgid "Open Preferences"
msgstr "Abrir Preferências"

#: ../data/ui/shortcutsWindow.xml:31
msgid "Quit"
msgstr "Sair"

#: ../data/ui/shortcutsWindow.xml:37
msgid "Refresh articles"
msgstr "Atualiza artigos RSS"

#: ../data/ui/shortcutsWindow.xml:43
msgid "Search articles"
msgstr "Procurar artigos"

#: ../data/ui/shortcutsWindow.xml:49
msgid "Next article"
msgstr "Próximo artigo"

#: ../data/ui/shortcutsWindow.xml:55
msgid "Previous article"
msgstr "Artigo anterior"

#: ../data/ui/shortcutsWindow.xml:61
msgid "Show/Hide read articles"
msgstr "Mostrar/Ocultar artigos lidos"

#: ../data/ui/shortcutsWindow.xml:67
msgid "Zoom in"
msgstr "Mais zoom"

#: ../data/ui/shortcutsWindow.xml:73
msgid "Zoom out"
msgstr "Menos zoom"

#: ../data/ui/shortcutsWindow.xml:79
msgid "Reset zoom"
msgstr "Restaurar zoom"

#: ../data/ui/manage_tags_popover_content.glade:24
msgid "New tag name..."
msgstr "Novo nome da tag ..."

#: ../data/ui/manage_tags_popover_content.glade:37
msgid "Add tag"
msgstr "Adicionar tag"

#: ../data/ui/manage_tags_popover_content.glade:112
msgid "There are no tags yet"
msgstr "Ainda não há tags"

#: ../data/ui/manage_tags_popover_content.glade:128
msgid "Add some using the entry above"
msgstr "Adicione  usando a entrada acima"

#: ../data/ui/scrolled_label.glade:21
#: ../data/ui/manage_tags_listbox_row_content.glade:18
msgid "label"
msgstr "legenda"

#: ../data/ui/article_right_click_popover_content.glade:48
msgid "Save article"
msgstr "Salvar artigo"

#: ../data/ui/menu.xml:6
msgid "Show read articles"
msgstr "Mostrar artigos lidos"

#: ../data/ui/menu.xml:10
msgid "Mark all as read"
msgstr "Marcar todos como lido"

#: ../data/ui/menu.xml:14
msgid "Mark all as unread"
msgstr "Marcar todos como não lido"

#: ../data/ui/menu.xml:24
msgid "Import OPML"
msgstr "Importar OPML"

#: ../data/ui/menu.xml:28
msgid "Export OPML"
msgstr "Exportar OPML"

#: ../data/ui/menu.xml:34
msgid "Preferences"
msgstr "Preferências"

#: ../data/ui/menu.xml:38
msgid "Keyboard Shortcuts"
msgstr "Atalhos do teclado"

#: ../data/ui/menu.xml:42
msgid "About Feeds"
msgstr "Sobre o Feeds"

#: ../data/ui/spinner_button.glade:23
msgid "page0"
msgstr "página0"

#: ../data/ui/spinner_button.glade:34
msgid "page1"
msgstr "página1"

#: ../data/ui/extra_popover_menu.glade:18
msgid "View mode"
msgstr "Modo de visualização"

#: ../data/ui/extra_popover_menu.glade:52
msgid "Reader Mode"
msgstr "Modo de leitura"

#: ../data/ui/extra_popover_menu.glade:67
msgid "RSS Content"
msgstr "Conteúdo RSS"

#: ../data/ui/headerbar.glade:34
msgid "Menu"
msgstr "Menu"

#: ../data/ui/headerbar.glade:66
msgid "Search"
msgstr "Pesquisa"

#: ../data/ui/headerbar.glade:86
msgid "Filter by feed"
msgstr "Filtro"

#: ../data/ui/headerbar.glade:118
msgid "Back to articles"
msgstr "Voltar aos artigos"

#: ../data/ui/headerbar.glade:134
msgid "Open externally"
msgstr "Abrir no navegador"

#: ../data/ui/headerbar.glade:154
msgid "Change view mode"
msgstr "Alterna o Modo de visualização"

#: ../data/ui/headerbar.glade:196
msgid "Share"
msgstr "Compartilhamento"

#: ../data/ui/add_feed_box.glade:20
msgid "Enter feed address to add"
msgstr "Digite o endereço RSS a ser adicionado"

#: ../data/ui/add_feed_box.glade:41
msgid "https://..."
msgstr "https://..."

#: ../data/ui/add_feed_box.glade:61
msgid "Add"
msgstr "Adicionar"

#: ../data/ui/add_feed_box.glade:91
msgid "You're already subscribed to that feed!"
msgstr "Você já está inscrito !"

#: ../data/ui/webview_filler.glade:42
msgid "Select an article"
msgstr "Selecione um artigo"

#: ../data/ui/webview_with_notification.glade:51
msgid "Link copied to clipboard!"
msgstr "Copiado para a área de transferência !"

#: ../data/org.gabmus.gfeeds.desktop.in:5
msgid "@prettyname@"
msgstr "@nomebonito@"

#: ../data/org.gabmus.gfeeds.desktop.in:6
msgid "News reader for GNOME"
msgstr "Leitor de notícias para o GNOME"

#: ../data/org.gabmus.gfeeds.desktop.in:8
msgid "@appid@"
msgstr "@appid@"

#: ../data/org.gabmus.gfeeds.desktop.in:13
msgid "rss;reader;feed;news;"
msgstr "leitor, feed, notícias;"

#: ../data/org.gabmus.gfeeds.appdata.xml.in:4
msgid "Feeds"
msgstr "Feeds"

#: ../data/org.gabmus.gfeeds.appdata.xml.in:15
msgid ""
"Feeds is a minimal RSS/Atom feed reader built with speed and simplicity in "
"mind."
msgstr ""
"Feeds é um leitor minimalista de notícias RSS / Atom construído para ser "
"rápido e simples."

#: ../data/org.gabmus.gfeeds.appdata.xml.in:16
msgid ""
"It offers a simple user interface that only shows the latest news from your "
"subscriptions."
msgstr ""
"Ele oferece uma interface simples que mostra apenas as últimas notícias de "
"suas assinaturas."

#: ../data/org.gabmus.gfeeds.appdata.xml.in:17
msgid ""
"Articles are shown in a web view by default, with javascript disabled for a "
"faster and less intrusive user experience. There's also a reader mode "
"included, built from the one GNOME Web/Epiphany uses."
msgstr ""
"Os artigos são mostrados em uma visualização Web por padrão, com o "
"javascript desativado para uma experiência mais rápida e menos invasiva. Há "
"também um modo de leitor, construído a partir do que o GNOME Web / Epiphany "
"usa."

#: ../data/org.gabmus.gfeeds.appdata.xml.in:18
msgid "Feeds can be imported and exported via OPML."
msgstr "As fontes RSS podem ser importadas e exportadas via OPML."

#~ msgid ""
#~ "Error resizing favicon for feed {0}. Probably not an image.\n"
#~ "Trying downloading favicon from an article."
#~ msgstr ""
#~ "Erro ao redimensionar o favicon para o feed {0}. Provavelmente não é uma "
#~ "imagem.\n"
#~ "Tentando baixar o favicon de um artigo."

#~ msgid ""
#~ "Error resizing favicon from article for feed {0}.\n"
#~ "Deleting invalid favicon."
#~ msgstr ""
#~ "Erro ao redimensionar o favicon do artigo para o feed {0}.\n"
#~ "Excluindo favicon inválido."

#~ msgid "Reader mode unavailable for this site"
#~ msgstr "Modo de leitor indisponível para este site"

#~ msgid "Show colored border"
#~ msgstr "Mostrar bordas coloridas"

#~ msgid "Enable client side decoration"
#~ msgstr "Ativar decoração de janela"

#~ msgid "There were problems with some feeds"
#~ msgstr "Ocorreram problemas com algumas fontes RSS"

#~ msgid "Gabriele Musco"
#~ msgstr "Gabriele Musco"

#~ msgid "Option to ellipsize article titles for a more compact view"
#~ msgstr ""
#~ "Opção de redimensionar títulos de artigos para uma visualização mais "
#~ "compacta"

#~ msgid "Added a search function"
#~ msgstr "Adicionada uma função de pesquisa"

#~ msgid "Updated dependencies"
#~ msgstr "Dependências atualizadas"

#~ msgid "Various UI improvements"
#~ msgstr "Várias melhorias na interface do usuário"

#~ msgid "Various bug fixes"
#~ msgstr "Várias correções de bugs"

#~ msgid "Errors with feeds are now shown in the UI"
#~ msgstr "Erros com feeds agora são mostrados na interface do usuário"

#~ msgid "Big UI overhaul"
#~ msgstr "Grande revisão da interface do usuário"

#~ msgid "Updated translations"
#~ msgstr "Traduções atualizadas"

#~ msgid "OPML file association"
#~ msgstr "Associação de arquivo OPML"

#~ msgid "Changed left headerbar button order"
#~ msgstr "Ordem dos botões da barra de cabeçalho esquerda alterada"

#~ msgid "Optimization for updating feeds"
#~ msgstr "Otimização para atualização de feeds"

#~ msgid "Redesigned right click/longpress menu"
#~ msgstr "Redesenhado clique direito /  menu"

#~ msgid "Option to show/hide read articles"
#~ msgstr "Opção para mostrar / ocultar artigos lidos"

#~ msgid "Reworked suggestion bar"
#~ msgstr "Barra de sugestões reformulada"

#~ msgid "Changed name to Feeds"
#~ msgstr "Nome alterado para Feeds"

#~ msgid "Improved CPU utilization"
#~ msgstr "Melhor utilização da CPU"

#~ msgid "New right click or longpress menu for articles"
#~ msgstr "Novo botão direito do mouse ou menu toque longo para artigos"

#~ msgid "You can now save articles offline"
#~ msgstr "Agora você pode salvar artigos offline"

#~ msgid "Initial suggestion to add feeds is now less intrusive"
#~ msgstr "A sugestão inicial de adicionar feeds agora é menos invasiva"

#~ msgid "Read articles are now greyed out"
#~ msgstr "Agora, os artigos lidos ficam acinzentados"

#~ msgid "Concurrent feeds refresh, with customizable thread count"
#~ msgstr ""
#~ "Atualização simultânea de feeds, com contagem de tópicos personalizável"

#~ msgid "Added German translation (thanks @Etamuk)"
#~ msgstr "Adicionado tradução alemã (Graças ao @Etamuk)"

#~ msgid "Fix bugs in reader mode"
#~ msgstr "Corrigido bugs no modo leitor"

#~ msgid "Minor bug fix"
#~ msgstr "Muitas correções de bugs"

#~ msgid "Improved date and time parsing and display"
#~ msgstr "Análise e exibição aprimoradas de data e hora"

#~ msgid "Reader mode can now work on websites without an article tag"
#~ msgstr "O modo Leitor agora pode funcionar em sites sem uma tag de artigo"

#~ msgid "Slight improvements to the icon"
#~ msgstr "Pequenas melhorias no ícone"

#~ msgid "New feature to filter articles by feed"
#~ msgstr "Novo recurso para filtrar artigos por feed"

#~ msgid "Improved favicon download"
#~ msgstr "Carregamento de favicon aprimorado"

#~ msgid "Fixed refresh duplicating articles"
#~ msgstr "Corrigida a atualização de artigos duplicados"

#~ msgid "Added option to disable client side decoration"
#~ msgstr "Adicionado opção para desativar a decoração da janela"

#~ msgid "Brought primary menu in line with GNOME HIG"
#~ msgstr "Trouxe o menu principal alinhado com o GNOME HIG"

#~ msgid "Added placeholder icon for feeds without an icon"
#~ msgstr "Ícone de marcador de posição adicionado para feeds sem um ícone"

#~ msgid "Migrated some widgets to Glade templates"
#~ msgstr "Migrado alguns widgets para modelos Glade"

#~ msgid "Option to use reader mode by default"
#~ msgstr "Opção para usar o modo leitor por padrão"

#~ msgid "Option to show article content from the feed"
#~ msgstr "Opção para mostrar o conteúdo do artigo a partir do feed"

#~ msgid "Fixed labels alignment"
#~ msgstr "Corrigido o alinhamento das etiquetas"

#~ msgid "Changed app name to Feeds"
#~ msgstr "Nome do aplicativo alterado para Feeds"

#~ msgid "Added separators for the two sections of the app"
#~ msgstr "Separadores adicionados para as duas seções do aplicativo"

#~ msgid "Using links as feed titles when there is no title"
#~ msgstr "Usando links como títulos de feed quando não há título"

#~ msgid "Added preference for maximum article age"
#~ msgstr "Adição de preferência para a idade máxima do artigo"

#~ msgid "Added zoom keyboard shortcuts"
#~ msgstr "Adicionados atalhos de teclado com zoom"

#~ msgid "Added preference to enable JavaScript"
#~ msgstr "Adicionado preferência para ativar o JavaScript"

#~ msgid "Fix window control positions when they are on the left"
#~ msgstr ""
#~ "Corrijido as posições de controle da janela quando estiverem à esquerda"

#~ msgid "Feeds for websites without favicons will now work"
#~ msgstr "Os feeds de sites sem favoritos agora funcionam"

#~ msgid "Fixed bug with adding new feeds"
#~ msgstr "Corrigido o erro ao adicionar novos feeds"

#~ msgid "Switched to native file chooser"
#~ msgstr "Mudado para o seletor de arquivo nativo"

#~ msgid "Added empty state initial screen for sidebar"
#~ msgstr "Adicionada tela inicial de estado vazio para a barra lateral"

#~ msgid "Added italian translation"
#~ msgstr "Adicionado tradução italiana"

#~ msgid "First release"
#~ msgstr "Primeiro lançamento"
